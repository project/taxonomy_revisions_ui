CONTENTS OF THIS FILE
------------------------

 * Introduction
 * Requirements
 * Installation
 * Usage
 * Maintainers


INTRODUCTION
---------------

This module provides an admin interface for viewing, reverting or deleting
Taxonomy term entity revisions.


REQUIREMENTS
---------------

This module is a visual enhancement for editing Taxonomy term entities, therefore a
Drupal core Taxonomy module is required.


INSTALLATION
---------------

Install as you would normally install a contributed Drupal module. Visit:
https://www.drupal.org/docs/extending-drupal/installing-modules
for further information.

CONFIGURATION
-------------

 - Navigate to Administration > Extend and enable the module.


USAGE
--------

No configuration is required.
When this module is installed and enabled, an additional "Revisions" tab is
visible when editing a Taxonomy term entity.


MAINTAINERS
--------------

Current maintainers:
 * Samit Khulve - https://www.drupal.org/u/samit310gmailcom

Supporting organisations:
 * Srijan Technologies - https://www.drupal.org/srijan-technologies
